#!/usr/bin/env bash
set -e

echo "[aws-cleanup] Cleanup $environment_name..."

# disable AWS CLI pager
export AWS_PAGER=""

sam delete --config-env "$environment_name" --no-prompts