# CloudFormation on AWS project sample

This project sample shows the usage of _to be continuous_ templates:

* AWS (Amazon Web Services)

The project deploys one single WebServer (Apache), based on AWS CloudFormation.

## AWS template features

This project uses AWS [CloudFormation](https://aws.amazon.com/cloudformation/) to create/update the infrastructure as well as the software (Apache).

This project uses the following features from the AWS template:

* Enables review environments by declaring the `$AWS_REVIEW_ENABLED` in the project variables,
* Enables staging environment by declaring the `$AWS_STAGING_ENABLED` in the project variables,
* Enables production environment by declaring the `$AWS_PROD_ENABLED` in the project variables.

The GitLab CI AWS template also implements [environments integration](https://gitlab.com/to-be-continuous/samples/maven-on-gcloud/environments) in GitLab:

* deployment environment integrated in merge requests,
* review environment cleanup support (manually or when the related development branch is deleted).

### implementation details

First of all, this project manages AWS authentication by specifying AWS credentials and required configuration as (secret) CI/CD project variables:

* `$AWS_ACCESS_KEY_ID`
* `$AWS_SECRET_ACCESS_KEY`
* `$AWS_DEFAULT_REGION`

:bulb: Depending on the case, we may have used a `$AWS_ROLE_ARN`variable too.

In order to perform AWS deployments, the project also implements:

* `aws-deploy.sh` script: generic deployment script using `aws cloudformation` commands,
* `aws-cleanup.sh` script: generic cleanup script using `aws cloudformation` commands.

All those scripts and descriptors make use of variables dynamically evaluated and exposed by the AWS template:

* `${environment_name}`: the application target name to use in this environment (ex: `myproject-review-fix-bug-12` or `myproject-staging`)
* `${environment_type}`: the environment type (`review`, `integration`, `staging` or `production`)

Lastly, the deployment script implements the [dynamic way](https://docs.gitlab.com/ee/ci/environments/#set-a-dynamic-environment-url) of
defining the environment URLs: retrieves the generated server URL as a [CloudFormation output](https://docs.aws.amazon.com/AWSCloudFormation/latest/UserGuide/outputs-section-structure.html), and dumps it into a `environment_url.txt` file, supported by the template.
