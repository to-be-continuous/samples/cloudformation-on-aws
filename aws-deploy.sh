#!/usr/bin/env bash
set -e

echo "[aws-deploy] Deploy $environment_name..."

# disable AWS CLI pager
export AWS_PAGER=""

template_file=file://Simple_WebServer.json
# retrieve environment type ($environment_type) from template
# retrieve $CI_JOB_URL and $CI_JOB_NAME from GitLab CI predefined variables
# retrieve $AWS_KEYPAIR from project secret variables
params_opts="--parameters ParameterKey=EnvType,ParameterValue=$environment_type ParameterKey=CiJobUrl,ParameterValue=$CI_JOB_URL ParameterKey=CiJobName,ParameterValue=$CI_JOB_NAME ParameterKey=KeyName,ParameterValue=$AWS_KEYPAIR_NAME"

if aws cloudformation describe-stacks --output text --stack-name "$environment_name" > /dev/null
then
  echo -e "Stack exists: update..."
  aws cloudformation update-stack --output text --stack-name "$environment_name" --template-body $template_file $params_opts

  echo "Waiting for stack to be updated..."
  aws cloudformation wait stack-update-complete --stack-name "$environment_name"
else
  echo -e "Stack doesn't exist: create..."
  aws cloudformation create-stack --output text --stack-name "$environment_name" --template-body $template_file $params_opts
  
  echo "Waiting for stack to be created..."
  aws cloudformation wait stack-create-complete --stack-name "$environment_name"
fi

# Retrieve outputs (use cloudformation query)
webserver_url=$(aws cloudformation describe-stacks --output text --stack-name "$environment_name" --query 'Stacks[0].Outputs[?OutputKey==`WebServerUrl`].OutputValue')
public_ip=$(aws cloudformation describe-stacks --output text --stack-name "$environment_name" --query 'Stacks[0].Outputs[?OutputKey==`WebServerPublicIp`].OutputValue')

echo "Stack created/updated:"
echo " - WebServerUrl: $webserver_url"
echo " - WebServerPublicIp: $public_ip"

# Finally set the dynamically generated WebServer Url
echo "$webserver_url" > environment_url.txt
